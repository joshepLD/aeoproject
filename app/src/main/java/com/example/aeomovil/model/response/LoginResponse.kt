package com.example.aeomovil.model.response

import com.example.aeomovil.model.models.Login
import com.google.gson.annotations.SerializedName

class LoginResponse (@SerializedName("0") var loginU: Login
)