package com.example.aeomovil.model.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Login (@SerializedName("id_usuario")@Expose var idUser: String,
             @SerializedName("id_operador")@Expose var idOpera: String,
             @SerializedName("usuario")@Expose var user: String,
             @SerializedName("password")@Expose var pass: String,
             @SerializedName("nombre")@Expose var name: String,
             @SerializedName("apellido_paterno")@Expose var apellidoP: String,
             @SerializedName("apellido_materno")@Expose var apellidoM: String,
             @SerializedName("operacionUsuario")@Expose var operacionU: String

)
