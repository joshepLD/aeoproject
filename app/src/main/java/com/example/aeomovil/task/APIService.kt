package com.example.aeomovil.task

import com.example.aeomovil.model.response.LoginResponse
import retrofit2.Call
import retrofit2.http.*

interface APIService {
    //http://www.poderonline.net/service/webServiceR/webservice_scp.php?funcion=validaUsrOperado
    @GET("webServiceR/webservice_scp.php?funcion=validaUsrOperador")
    fun login(@Query("usr") usuario:String, @Query("psw") password:String) : Call<LoginResponse>
}

//http://www.poderonline.net/service/webServiceR/webservice_scp.php?funcion=validaUsrOperador&test
// =tes