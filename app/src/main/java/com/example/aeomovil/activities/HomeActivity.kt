package com.example.aeomovil.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.aeomovil.R
import com.example.cognitus.utilities.DialogoAlerta

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        DialogoAlerta.crearDialogo(this,"Bienvenido:", "Jose ")
    }
}
