package com.example.aeomovil.activities

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View.VISIBLE
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.example.aeomovil.R
import com.example.aeomovil.model.response.LoginResponse
import com.example.aeomovil.task.APIService
import com.example.aeomovil.utilities.Utils
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_login_detalle.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class LoginActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        edtContraseña.requestFocus()
        edtUsuario.requestFocus()

        edtContraseña.setOnClickListener {
            enableUIElements(true)

        }
        edtUsuario.setOnClickListener {
            enableUIElements(true)

        }
        btnIniciar.setOnClickListener {
            if(edtUsuario.text!!.isEmpty() || edtContraseña.text!!.isEmpty()){
                Toast.makeText(this, "Los campos estan vacios", Toast.LENGTH_LONG).show()
            }else{
                if(Utils.veryfyAvailableNetwork(this)){
                    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
                    Log.e("Tag", "Paso1")

                    Log.e("Tag", edtUsuario.text.toString())
                    Log.e("Tag", edtContraseña.text.toString())

                    searchLogin(edtUsuario.text.toString(), edtContraseña.text.toString())

                }else{

                    Toast.makeText(this, "No cuenta con conexion a internet", Toast.LENGTH_LONG).show()
                }
            }

        }

    }
    private fun searchLogin(usr:String, pass:String){
        Log.e("Tag", "Paso2")
        doAsync{
            val call = getRetrofit().create(APIService::class.java).login(usr, pass).execute()
            val response = call.body() as LoginResponse
            Log.e("Tag", "--->"+response)
            uiThread {
                if(response.loginU.idUser == "1"){
                    Log.e("Tag", "Paso4")
                    val intent = Intent(this@LoginActivity, HomeActivity::class.java)
                    startActivity(intent)
                    finish()
                }else{
                  //  Log.e("Error", "--->"+response)
                    //showErrorDialog()
                    }
            }
        }
    }

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(getString(R.string.base_url))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun enableUIElements(enable: Boolean){
        app_bar.setExpanded(!enable)
        activity_Detalle.isNestedScrollingEnabled = !enable

        btnIniciar.visibility = VISIBLE

        //edtLContraseñaD.visibility = VISIBLE

    }

}
